package main

import(
    "fmt"
    "net"
    "log"
    "time"
    "bufio"
    "strings"
)

func echo(c net.Conn, shout string, delay time.Duration)  {
    fmt.Fprintln(c, "\t", strings.ToUpper(shout))
    time.Sleep(delay)
    fmt.Fprintln(c, "\t", shout)
    time.Sleep(delay)
    fmt.Fprintln(c, "\t", strings.ToLower(shout))
}

func handleConn(c net.Conn)  {
    input := bufio.NewScanner(c)
    for input.Scan() {
        echo(c, input.Text(), 1*time.Second)
    }
    c.Close()
}

func Run() {
    listener, err := net.Listen("tcp", "localhost:8000")
    if err != nil {
        log.Fatal(err)
    }
    for {
        // Accept method blocks until an incoming connection request is made
        conn, err := listener.Accept()
        if err!= nil {
            log.Print(err)
            continue
        }
        go handleConn(conn)
    }
}

func main()  {
    Run()
}
