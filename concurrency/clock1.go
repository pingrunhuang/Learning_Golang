package main

import (
    "io"
    "log"
    "net"
    "time"
)

// this program plays the role of server app
func Run()  {
    listener, err := net.Listen("tcp", "localhost:8000")
    if err != nil {
        log.Fatal(err)
    }
    for {
        // Accept method blocks until an incoming connection request is made
        conn, err := listener.Accept()
        if err!= nil {
            log.Print(err)
            continue
        }
        go handleconn(conn)
    }
}

func handleconn(c net.Conn)  {
    defer c.Close()
    for  {
        // write the formated time to the connection
        _, err := io.WriteString(c, time.Now().Format("15:04:05\n"))
        // the loop end when the write failed
        if err!=nil {
            return
        }
        time.Sleep(1*time.Second)
    }
}

func main()  {
    Run()
}
