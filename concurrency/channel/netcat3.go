package main

import(
    "io"
    "log"
    "os"
    "net"
    "flag"
)
var ip = flag.String("ip", "localhost", "the server address to connect to")
var port = flag.String("port", "8000", "the port that the server listen to")

func transfer(dst io.Writer, src io.Reader)  {
    if _, err := io.Copy(dst, src); err!=nil {
        log.Fatal(err)
    }
}

func main()  {
    flag.Parse()
    conn, err := net.Dial("tcp", *ip + ":" + *port)
    if err!=nil {
        log.Fatal(err)
    }
    done := make(chan struct{})
    go func() {
        io.Copy(os.Stdout, conn)
        log.Println("done")
        // using a struct{} as the type of the channel when we just want the channel to fullfill the sole purpose of syncronization
        done <- struct{}{}
    }()
    transfer(conn, os.Stdin)
    // the conn is an implementation of *net.TCPConn which consists of CloseWrite and CloseRead method
    // modify the close conn to only close the write part
    if tcpconn, ok := conn.(*net.TCPConn); ok {
        tcpconn.CloseWrite()
    }
    // wait for background goroutine to finish
    <-done
}
