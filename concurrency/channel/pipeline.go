package main

import(
    "fmt"

)

func channelProcess()  {
    naturals := make(chan int)
    squares := make(chan int)

    go func ()  {
        for x := 0; x < 100; x++ {
            naturals <- x
        }
        close(naturals)
    }()

    go func() {
        for x := range naturals {
            squares <- x * x
        }
        close(squares)
    }()

    for x := range squares {
        fmt.Println(x)
    }
}


// define a func with input channel as a parameter
func counter(out chan<- int)  {
    count := 100
    for i := 0; i < count; i++ {
        out<- i
    }
    close(out)
}

// how to define a receive only channel
func squarer(in <-chan int, out chan<- int)  {
    for x := range in {
        out <- x * x
    }
    close(out)
}

func printer(in <-chan int)  {
    for x := range in {
        fmt.Println(x)
    }
}

func runWithFunc()  {
    count := make(chan int)
    square := make(chan int)
    go counter(count)
    go squarer(count, square)
    printer(square)
}

func main()  {
    runWithFunc()
}
