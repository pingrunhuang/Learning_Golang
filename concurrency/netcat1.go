package main

import (
    "net"
    "io"
    "log"
    "os"
)

// customized version of nc (netstat) using net.Dial to connect to the server
var ip = "localhost"
var port = "8000"

func copyToStdout(dst io.Writer, src io.Reader)  {
    // this function copy the data transfered from a given connection to local
    if _, err := io.Copy(dst, src); err != nil {
        log.Fatal(err)
    }
}

func Run()  {
    conn, err := net.Dial("tcp", ip + ":"+port)
    if err!=nil {
        log.Fatal(err)
    }
    defer conn.Close()
    netcat(os.Stdout, conn)
}

func main()  {
    Run()
}
