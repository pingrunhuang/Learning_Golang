package main

import (
    "net"
    "fmt"
    "log"
    "io"
    "flag"
    "time"
)


var port = flag.String("port", "8000", "the port that this app will serve on")
var timezone = flag.String("timezone", "Asia/China", "the virtual time zone this server app runs in")

func Run()  {
    listener, err := net.Listen("tcp", "localhost:8000")
    if err != nil {
        log.Fatal(err)
    }
    for {
        // Accept method blocks until an incoming connection request is made
        conn, err := listener.Accept()
        if err!= nil {
            log.Print(err)
            continue
        }
        go handleconn(conn)
    }
}

func handleconn(c net.Conn)  {
    defer c.Close()
    for  {
        // write the formated time to the connection
        _, err := io.WriteString(c, time.Now().Format("15:04:05\n"))
        // the loop end when the write failed
        if err!=nil {
            return
        }
        time.Sleep(1*time.Second)
    }
}


func main()  {
    // Run()
    flag.Parse()
    location, err := time.LoadLocation(*timezone)
    // dt, err := time.ParseInLocation("15:04:05", location)
    if err != nil {
        fmt.Printf("%v\n", err)
    }

    fmt.Printf("%s", location)

}
