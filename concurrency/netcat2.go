package main

import  (
    "net"
    "flag"
    "os"
    "io"
    "log"
)
// this program let the user can type the chat content while not blocked by receiving respond from server

var ip = flag.String("ip", "localhost", "the server address to connect to")
var port = flag.String("port", "8000", "the port that the server listen to")

func transfer(dst io.Writer, src io.Reader)  {
    if _, err := io.Copy(dst, src); err!=nil {
        log.Fatal(err)
    }
}

func main()  {
    flag.Parse()
    conn, err := net.Dial("tcp", *ip + ":" + *port)
    if err!=nil {
        log.Fatal(err)
    }
    defer conn.Close()
    go transfer(os.Stdout, conn)
    transfer(conn, os.Stdin)
}
