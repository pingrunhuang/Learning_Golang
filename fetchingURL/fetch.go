package main

import (
    "fmt"
    // "io/ioutil"
    "net/http"
    "os"
    "io"
    "strings"
)

func main()  {
    for _, urlStr := range os.Args[1:] {
        if !strings.HasPrefix(urlStr, "http://") {
            urlStr = "http://" + urlStr
        }

        response, err := http.Get(urlStr)
        if err != nil {
            fmt.Fprintf(os.Stderr, "fetch:%s, %v\n", err)
            os.Exit(1)
        }

        // this way is keeping the response body in the buffer memory
        // body, err := ioutil.ReadAll(response.Body)

        // this way is printing out the body immediately
        body := os.Stdout
        _, err = io.Copy(body, response.Body)
        response.Body.Close()
        if err != nil {
            fmt.Fprintf(os.Stderr, "fetch:%s, %v\n", err)
            os.Exit(1)
        }
        fmt.Printf("status: %s\n", response.Status)
        fmt.Printf("%s\n", body)
    }
}
