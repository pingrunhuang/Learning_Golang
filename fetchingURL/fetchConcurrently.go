package main

import (
    "fmt"
    "os"
    "io"
    "io/ioutil"
    "net/http"
    "time"
)

func main()  {
    urls := os.Args[1:]
    start := time.Now()
    // A channel is a communication mechanism that allows one goroutine to pass values of a specified type to another goroutine
    ch := make(chan string)
    for _, url := range urls {
        go fetch(url, ch)
    }
    fmt.Printf("%.2fs elapsed\n", time.Since(start).Seconds())
}

func fetch(url string, ch chan<-string)  {
    start := time.Now()
    resp, err1 := http.Get(url)
    if err1 != nil {
        // pass the string to a channel
        ch <- fmt.Sprint(err)
        return
    }
    nbytes, err2 = io.Copy(ioutil.Discard, resp.Body)
    if err2!=nil {
        ch <- fmt.Sprintf("While reading %s:%v \n", url, err)
        return
    }
    secs := time.Since(start).Seconds()
    ch <- fmt.Sprintf("%.2fs %7d %s", secs, nbytes, url)
}
