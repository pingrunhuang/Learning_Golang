# Channels
在go并发编程的时候，有一个叫做`Channel`的概念是跟java的并发编程很不一样的。Java里面为了保证变量在多线程中的一致性和防止死锁的情况发生，引入了很多带锁性质的变量，这对于猴子来说是一件很苦恼的事情，有时候莫名其妙就出现死锁，莫名其妙就数据不一致了，这其实要求猴子们对java内置的数据类型非常了解，以至于至今我也还在学习。

但是GO引入了一个`Channel`的概念，大大的降低了并发编程的难度。下面我们来看看`Channel`的一些实现和使用方法。`The go programming language`书中提到了`Channel`相当于在不同的`Goroutine`之间架起的桥梁，每一个`Channel`ui有一个变量类型，这个变量类型表示这个`Channel`只能传输这样的数据类型。

## declaration
`ch := make(chan int, capacity int)`: This cause the ch to have type `chan int` which is also a reference. This initialize the `ch` to equal `nil`(the null value in GO). `capacity` determine if the channel is buffered or not(0).

声明一个`receive channel`可以用`chan<- int`，声明一个`send channel`可以用`<-chan int`.

## action
- send: `ch <- x`
- receive: `x = <-ch` or `<-ch` for stashing  
_always remember to close the channel using `close(ch)`._  


## Summary
说了这么多用法，我自己在使用的过程中有一些想法或者说对channel的比喻，如果把channel比作沟通不同goroutine之间的桥梁的话，那变量就是走这个桥的人，默认情况下，一个人徒步走这个桥肯定是很低效的，所以需要有一辆巴士，这辆巴士就是`Buffered channel`了，有了`Buffered channel`之后让人去走这个桥就会容易很多，一次可以通行的人也多了，效率自然提升了。

初看起来，`Buffered channel`很像是一个队列，有着先进先出的特性，但是作者Alan和Brian(为了表示专业，后文直接称呼Alan和Brian)也提到，这样的想法其实是不对的，如果要实现队列的效果，应该考虑用`slice`来实现。
