很遗憾，原本写这个系列文章是想每天写一篇来记录自己学go的过程的，结果第一个周末就打脸了，没关系，再继续加油坚持就好了。还有本来是希望用英语写的（练英语写作和装逼），但是后来想，还是算了，毕竟这是在国内啊，还是用母语清晰的表达
清楚自己的观点比较重要。后期如果真要翻译，再考虑调用一下`google`的`translate API`来实现翻译的功能吧。话不多说，直接开始搬砖。
这个小节，我主要总结一下关于Go里面的一些collection类型。

# The new function
- syntax: `p := new(int)` create a type `*int` which is a pointer points to an unnamed int variable


# Type declaration
Type declaration is in the form of `type name underline_type`. With this syntax, you can define your new type with the customized name while having a underlying type such as float64 or int etc.
Notice to export the newly created type, you just have to define it starting with upper case.


# Strings
- utf-8
- utf
- ascii


# 变量的生命周期
> how the garbage collector determines whether a variable should be allocated or not?
If the variable is reachable in the package level, then it won't get allocated. Else, it will be allocated by the garbage collector. By saying reachable, I mean you can't access the value later on after
this assignment. 这部分是我在看书过程中直接写下来的记录，从书中的解释可以产出go的gc处理机制跟java有点相似。

# 赋值
- 声明类型：`var a int`
- 自动定义类型并赋值: `a := 1`
- Tuple assignment: This is quite similar to python usage. With this syntax, it is quite easy to write some classic algo.
```go
func gcd(x, y int) int {
  for y!= 0 {
    x, y = y, x%y
  }
  return x
}

func fibonacci(n int) int {
  x,y := 0,1
  for i:=0; i < n; i++ {
    x, y = y , x + y
  }
  return x
}
```

# Composite type

## Arrays
- Declaration: `var a [3]int`, `array := [...]string{1:"value1", 2:"value2"}`
- Comparison: 如果array里面的元素都是可比的，那`==`将返回array里面每个元素都相等的结果，也就是所有元素都相等的话，`==`会返回true，否则false。前提是长度必须相等，否则编译错误。
-
