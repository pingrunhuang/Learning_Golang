package main

import (
    "bufio"
    "fmt"
    "os"
)

func main() {
    // create a map with key as string and value as int
    counts := make(map[string]int)
    // in this case, file path should be followed by '<' syntax
    input := bufio.NewScanner(os.Stdin)
    for input.Scan() {
      counts[input.Text()]++
    }

    for line, count := range counts {
      if count > 1 {
        fmt.Printf("%d\t%s\n", count, line)
      }
    }
}
