package main

import (
    "os"
    "io/ioutil"
    "fmt"
    "strings"
)

func main(){
    counts := make(map[string]int)
    files := os.Args[1:]
    for _, val := range files {
        data, err := ioutil.ReadFile(val)
        if err != nil {
            fmt.Fprintf(os.Stderr, "dup3: %v\n", err)
            continue
        }
        // the data is originally in the byte array formate which is why we have to convert it into string first
        for _, line := range strings.Split(string(data), "\n") {
            counts[line]++
        }
    }

    for val, count := range counts {
        fmt.Printf("%d\t%s\n", count, val)
    }

}
