package main

// This snippet is going to read from a list of files using the os.Open

import (
    "os"
    "fmt"
    "bufio"
)

// the counts is passed as a reference, hence the change to the counts inside this funciton will impact the global value
func countLines(f *os.File, counts map[string]int) {
    input := bufio.NewScanner(f)
    for input.Scan() {
        counts[input.Text()]++
    }
}

func main() {
    counts := make(map[string]int)
    files := os.Args[1:]

    if len(files) == 0 {
        countLines(os.Stdin, counts)
    } else {
        for _, value := range files {
            // the os.Open method return 2 variables with *os.File and
            file, err := os.Open(value)
            if err != nil {
                // error checking and how to print out error
                fmt.Fprintf(os.Stderr, "duplicated 2: %v/n", err)
                continue
            }
            countLines(file, counts)
            file.Close();
        }
    }

    for line, count := range counts {
        fmt.Printf("%d\t%s\n", count, line)
    }

}
