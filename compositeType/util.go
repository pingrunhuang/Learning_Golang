package Util

func equalMap(x, y map[string]int ) bool  {
    if len(x) != len(y) {
        return false
    }
    for kx, vx := range x {
        // if the y[kx] does not exist or equal 0 then the ok is false
        if vy, ok := y[kx]; !ok || vy != vx {
            return false
        }
    }
    return true
}
