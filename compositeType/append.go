package main

import "fmt"
// In this program, what we have to really know is that an array is composed with 3 basic info
// Therefore, creating an array should pass in 3 arguments
func AppendInt(x []int, y int) []int {
    var z []int
    zlen := len(x) + 1
    if zlen <= cap(x) {
        // slice the x to assign to z
        z = x[:zlen]
    } else {
        // otherwise double the capacity of z
        zcap := zlen
        if zcap < 2*len(x) {
            zcap = 2*len(x)
        }
        // initialize a new array with zero values
        z = make([]int, zlen, zcap)
        // built in function to copy the src to dst (x -> y), return the number of elements being copied
        copy(z, x)
    }
    z[len(x)] = y
    return z
}

func run()  {
    var x, y []int
    for index := 0; index < 10; index++ {
        y = AppendInt(x, index)
        // %v means the value in a default format
        fmt.Printf("%d\tcap=%d\t%v\n", index, cap(y), y)
        x = y
    }
}

func main()  {
    run()
    
    // var str []int
    // for i := 0; i < 10; i++ {
    //     str = append(str, i)
    //     fmt.Printf("%v\n", str)
    // }

}
